import pandas as pd
from random import random
import numpy as np
import matplotlib.pyplot as plt
from sklearn.svm import SVC, LinearSVC

train_file = pd.read_csv("train.csv")

#Populating training and validation sets
train_images = []
train_labels = []
validate_images = []
validate_labels = []

for line in train_file.values:
    x = line[1:]
    x = [1 if i < 50 else 0 for i in x]
    if random() < 0.75:
        train_labels.append(line[0])
        train_images.append(x)
    else:
        validate_labels.append(line[0])
        validate_images.append(x)

train_images = np.array(train_images)
train_labels = np.array(train_labels)

validate_images = np.array(validate_images)
validate_labels = np.array(validate_labels)

#Visualizing the image
# arr = train_images[0]
# arr.resize((28, 28))
# plt.imshow(arr, cmap="gray")
# plt.show()

#Training
fit_obj = SVC(kernel='rbf', C = 10, gamma=0.003)
fit_obj.fit(train_images, train_labels)

#Validating
print("Accuracy: " + str(fit_obj.score(validate_images, validate_labels)*100) + "%")

# Creating CSV file with predicted values
predicted_values = fit_obj.predict(validate_images)

f = open("predicted_values.csv", "w+")

for i in predicted_values:
    f.write(str(i) + "\r\n")

f.close()